# OpenAI in rust
This crate allows you to create an environment in OpenAI where to train your agents.

## Setup
To use this crate you will need to have a few things already installed on your machine including python 3 as well as OpenAI gym and the environments you need. You can find out how to install OpenAI in the [README](https://github.com/openai/gym#id8) of their repository.

Once you've installed all of the dependencies you'll need to start a display server on port zero like this:
```
xvfb-run -s "-screen 0 1080x1920x24" bash
export DISPLAY=:0
```
Now that you've executed this command you can run your program and a window should pop up and you'll see your agent training in the environment that you have chosen.

## Code example
Cargo.toml
```toml
[dependencies]
open_ai = "0.1.1"
rand = "0.3"
```

main.rs
```rust
extern crate open_ai;
extern crate rand;

use rand::Rng;
use open_ai::{ Python, Env, PyDict };

fn main() {
    let gil = Python::acquire_gil();

    // Create your environment
    let env = Env::make(gil.python(), "LunarLander-v2");

    let mut observation = env.reset();
    loop {
        env.render();

        // Here goes your agent
        let action: i32 = rand::thread_rng().gen_range(0,4);

        let result = env.step(action);
        observation = result.0;

        if result.2 {
            observation = env.reset();
        }
    }
}
```